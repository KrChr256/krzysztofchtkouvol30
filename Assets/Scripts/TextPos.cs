﻿using UnityEngine;
using System.Collections;

public class TextPos : MonoBehaviour {

	public Transform lookAtObj;

	public Vector3 startPosition;

	// Use this for initialization
	void Start () {
	
		startPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	
		transform.position = startPosition + lookAtObj.position;
	}


}
