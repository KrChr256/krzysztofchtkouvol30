﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Home : MonoBehaviour {

	public GameObject textObj;
	Text txt;

	public int shroomerNow;
	public int shroomerMax;


	public GameObject goOutsideButton;


	// Use this for initialization
	void Start () {

		goOutsideButton = GameObject.Find ("Button05");
	
		shroomerMax = Random.Range (1, 5);
	}
	
	// Update is called once per frame
	void Update () {

		txt = textObj.GetComponent<Text>(); 
		txt.text = shroomerNow + "/" + shroomerMax;
	
		if (goOutsideButton.GetComponent<GoOutside> ().button == true)
		{
			shroomerNow = 0;
		}
	}

}
