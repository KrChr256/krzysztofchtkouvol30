﻿using UnityEngine;
using System.Collections;

public class AddHome : MonoBehaviour {

	
	public Rigidbody homePrefab;
	
	
	public float positionXMin = -5f;
	public float positionXMax = 5f;

	public float positionY = 0f;

	public float positionZMin = -5f;
	public float positionZMax = 5f;

	
	// Use this for initialization
	void Start () {
		
		//	transform.localScale = new Vector3(size, size, size);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	
	
	void OnMouseDown()
		
	{
		//Instantiate (shroomPrefab, firePosition.position, firePosition.rotation); // as Rigidbody;
		
		Instantiate (homePrefab, new Vector3(Random.Range(positionXMin, positionXMax),-1,Random.Range(positionZMin, positionZMax)) , Quaternion.Euler(0, 0, 0)); // as Rigidbody;
		
	}
	
	
	
}