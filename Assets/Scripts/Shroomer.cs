﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Shroomer : MonoBehaviour {

	public float moveSpeed = 1f;

	public float timer;
	public float timerRotate = 0f;
	public float timerRotateReset = 0f;


	public float timerDirection;

	public int direction = 0;

	public int shroomCounter = 0;

	public float positionX = 0f;
	public float positionY = 0f;
	public float positionZ = 0f;
	public bool moveAlloved = true; 


	public GameObject goHomeButton;
	public GameObject goOutsideButton;
	public GameObject textObj;
	Text txt;


	public bool goHome = false;
	public bool goOutside = true;

	public bool moveAllovedHome = false; //true if Shroomer is inside the home

	Vector3 myPosition;
	Vector3 directionRay;
	RaycastHit rch_def;

	float distanceRay = 0f;





	// Use this for initialization
	void Start () {


		goHomeButton = GameObject.Find ("Button04");
		goOutsideButton = GameObject.Find ("Button05");

		distanceRay = Random.Range(4f, 10f);
		positionX = Random.Range(-5f, 5f);
		positionZ = Random.Range(-5f, 5f);

		moveSpeed = Random.Range(0.2f, 1f);
		
		transform.position = new Vector3(positionX, -1f, positionZ);
	
	}



	// Update is called once per frame
	void Update () 
	{

		FindShroom (); 	//raycast

		Border (); 		//go back

		goHome = goHomeButton.GetComponent<GoHome> ().button;
		//goHome = !goOutsideButton.GetComponent<GoOutside> ().button;
		//goOutside = 

		goOutside = goOutsideButton.GetComponent<GoOutside> ().button;



		txt = textObj.GetComponent<Text>(); 
		txt.text = "" + shroomCounter;


		if (goOutside)//(goOutsideButton ) //if [Go Outside] button is active
			moveAllovedHome = false;


		if (moveAllovedHome == false) // true if [Go Home] Button is active
		{
			transform.Translate (Vector3.forward * moveSpeed * Time.deltaTime);
		}




		if (timerDirection > 1) 
		{
			direction = (int) (Random.Range(1f, 3f));
			timerDirection = 0f;
		}





		if (moveAlloved == true) 
			{
			switch (direction) {
			case 1:
				{
			
					transform.Rotate (Vector3.up, 100f * moveSpeed * Time.deltaTime);
			
					//Random.Range(1f, 2f);
				}
				break;


			case 2:
				{
			
					transform.Rotate (Vector3.up, -100f * moveSpeed * Time.deltaTime);
			
					//Random.Range(1f, 2f);
				}
				break;

			case 3:
			
				break;

			}

		}

		/*
		//if (timerRotate > Random.Range(0.5f, 2f))
		    {

			transform.Rotate(Vector3.up, 50f*Time.deltaTime);

			//Random.Range(1f, 2f);
		}*/


	}

	void FixedUpdate ()
	{
		timerDirection = timerDirection + Time.deltaTime;
		timerRotate = timerRotate + Time.deltaTime;
	}




	
	void OnTriggerEnter(Collider other)
	{

		if (other.tag == "Shroom")
		{
			shroomCounter = shroomCounter + 1;
			moveAlloved = true;
			Destroy (other.gameObject);
		}


		if (other.tag == "Home" && goHome == true ) //&& moveAllovedHome == false
		{
			//shroomCounter = shroomCounter + 1;
			//moveAllovedHome = true;


			if (other.gameObject.transform.root.GetComponent<Home>().shroomerNow < other.gameObject.transform.root.GetComponent<Home>().shroomerMax)
			    {
				other.gameObject.transform.root.GetComponent<Home>().shroomerNow++; 
				moveAllovedHome = true;
			}
			//
			//Destroy (other.gameObject);
		}


	}



	void Border()
	{
		
		if (transform.position.x > 8f)   //(transform.position.x > 30f) 
		{
			transform.Rotate (Vector3.up * Time.deltaTime * 100f, Space.World);
			
			
			//Debug.Log ("transform.position.x = " + transform.position.x);
		}
		
		
		if (transform.position.x < -8f)  //(transform.position.x < -30f) 
		{
			transform.Rotate (-Vector3.up * Time.deltaTime * (100f), Space.World);
			
			
			//Debug.Log ("transform.position.x = " + transform.position.x);
		}
		
		
		
		if (transform.position.z < -6f)  // (transform.position.z < -20f) 
		{
			transform.Rotate (Vector3.up * Time.deltaTime * (100f), Space.World);
			
			
			//Debug.Log ("transform.position.x = " + transform.position.x);
		}
		
		
		
		if (transform.position.z > 6f)  // (transform.position.z > 20f) 
		{
			transform.Rotate (-Vector3.up * Time.deltaTime * (100f), Space.World);
			
			
			//Debug.Log ("transform.position.x = " + transform.position.x);
		}
	}





	
	void FindShroom()
	{
		myPosition = transform.position;
	
		
		directionRay = new Vector3 (0, 0, 1);

		Ray ray = new Ray (myPosition, transform.forward); 

		RaycastHit rch;
		
										//distanceRay
		if (Physics.Raycast (ray, out rch, distanceRay)) {
			//         ja > obiekt na celowniku
			if (rch.collider.tag == "Shroom" && goHome == false) //&& goHome == false
			{ 

				Debug.DrawLine (transform.position, rch.point, Color.black);
				//Destroy (rch);
				//Debug.Log ("  Reycast hit = " + rch.collider.tag);
				
				
				moveAlloved = false;
			}




			if (rch.collider.tag == "Home" && goHome == true) 
			{ 
				
				Debug.DrawLine (transform.position, rch.point, Color.red);
				//Destroy (rch);
				//Debug.Log ("  Reycast hit = " + rch.collider.tag);
				
				
				moveAlloved = false;
			}
			
		}
	}



}
