﻿using UnityEngine;
using System.Collections;

public class Shroom : MonoBehaviour {


	public float size = 0f;

	public float speedSize = 0f;

	public float positionX = 0f;
	public float positionY = 0f;
	public float positionZ = 0f;



	public float timer = 0f;

	// Use this for initialization
	void Start () {
	
	
		size = Random.Range (0.1f, 0.5f);
		speedSize = Random.Range(0.5f, 2.0f);

		positionX = Random.Range(-5f, 5f);
		positionZ = Random.Range(-5f, 5f);

		transform.position = new Vector3(positionX, -1f, positionZ);
		transform.localScale = new Vector3(size, size, size);
	}


	
	// Update is called once per frame
	void Update () 
	{
	
		if (transform.localScale.x < 2f)
		transform.localScale = new Vector3 (size+(timer*0.05f*speedSize), size+(timer*0.05f*speedSize), size+(timer*0.05f*speedSize));
	}


	void FixedUpdate ()
	{
		timer = timer + Time.deltaTime;
	}


}
